const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
  getAll() {
    return FighterRepository.getAll();
  }

  create(fighterData) {
    const fighter = FighterRepository.getOne({name: fighterData.name})
    if (fighter) {
      throw Error("Fighter with that name exists");
    }
    return FighterRepository.create(fighterData);
  }

  remove(id) {
    const fighter = FighterRepository.getOne({id});
    if (!fighter) {
      throw Error("Fighter does not exist");
    }
    return FighterRepository.delete(id);
  }

  userUpdate(id, dataToUpdate) {
    const fighter = FighterRepository.getOne({id});
    if (!fighter) {
      throw Error("User does not exist");
    }
    return FighterRepository.update(id, dataToUpdate)
  }

  getOne(id) {
    const fighter = FighterRepository.getOne({id});
    if (!fighter) {
      throw Error("Fighter does not exist");
    }
    return fighter;
  } 
}

module.exports = new FighterService();