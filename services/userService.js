const { UserRepository } = require('../repositories/userRepository');

class UserService {

  create(userData) {
    const user = UserRepository.getOne({email: userData.email});
    if (user) {
      throw Error("User with email is registered");
    }
    return UserRepository.create(userData);
  }

  getOne(id) {
    const user = UserRepository.getOne({id});
    if(!user) {
      return null;
    }
    return user;
  }

  remove(id) {
    const user = UserRepository.getOne({id});
    if (!user) {
      throw Error("User does not exist");
    }
      return UserRepository.delete(id);
  }

  getAll() {
    return UserRepository.getAll();
  }
     
  userUpdate(id, dataToUpdate) {
    const user = UserRepository.getOne({id});
    if (!user) {
      throw Error("User does not exist");
    }
    return UserRepository.update(id, dataToUpdate);
  }

  search(search) {
    const item = UserRepository.getOne(search);
    if(!item) {
      return null;
    }
    return item;
  }
}

module.exports = new UserService();