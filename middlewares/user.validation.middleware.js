const { user } = require('../models/user');
const {Joi, validate, ValidationError, } = require('express-validation');

const gmailValid = (value, helpers) => {
  if (!(value.slice(-10) === "@gmail.com")) {
    throw new Error("Email must be gmail");
  }
  return value;
};

const validUser = {
  body: Joi.object({
    firstName: Joi.string()
      .not().empty()
      .min(2)
      .max(20)
      .required(),
    lastName: Joi.string()
      .not().empty()
      .min(3)
      .max(20)
      .required(),
    email: Joi.string()
      .email()
      .custom(gmailValid,"Gmail validation")
      .required()
      .messages({
        'any.custom': `Email must be gmail`
      }),
    phoneNumber: Joi.string()
      .regex(/\+380\d\d\d\d\d\d\d\d\d/)
      .required()
      .messages({
        'string.pattern.base': `Enter a phone number of type +380XXXXXXXXX`,
      }),
    password: Joi.string()
      .not().empty()
      .min(2)
      .max(20)
      .required(),
  })
}

const updateUser = {
  body: Joi.object({
    firstName: Joi.string()
      .not().empty()
      .min(2)
      .max(20),
    lastName: Joi.string()
      .not().empty()
      .min(3)
      .max(20),
    email: Joi.string()
      .email()
      .custom(gmailValid,"Gmail validation")
      .messages({
        'any.custom': `Email must be gmail`
      }),
    phoneNumber: Joi.string()
      .regex(/\+380\d\d\d\d\d\d\d\d\d/)
      .messages({
        'string.pattern.base': `Enter a phone number of type +380XXXXXXXXX`,
      }),
    password: Joi.string()
      .not().empty()
      .min(2)
      .max(20),
  })
}



const createUserValid = [
  validate(validUser,{},{}),
  (err, req, res, next) => {
    const message = err.details.body[0].message
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json({error: true, message: message })
    }
    next()
  },
  (req, res, next) => {
    res.data = Object.assign({}, user, req.body);
    next();
  }
];

const updateUserValid = [
  validate(updateUser,{},{}),
  (err, req, res, next) => {
    const message = err.details.body[0].message
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json({error: true, message: message })
    }
    next();
  }
];

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;