const { fighter } = require('../models/fighter');
const {Joi, validate, ValidationError, } = require('express-validation');

const validFighter = {
  body: Joi.object({
    name: Joi.string()
      .not().empty()
      .min(2)
      .max(15)
      .required(),
    defense: Joi.number()
      .not().empty()
      .min(1)
      .max(99)
      .required(),
    power: Joi.number()
      .not().empty()
      .min(1)
      .max(99)
      .required(),
  })
}

const updateFighter = {
  body: Joi.object({
    name: Joi.string()
      .not().empty()
      .min(2)
      .max(15),
    defense: Joi.number()
      .not().empty()
      .min(1)
      .max(99),
    power: Joi.number()
      .not().empty()
      .min(1)
      .max(99),
  })
}

const createFighterValid = [
  validate(validFighter, {}, {}),
  (err, req, res, next) => {
    const message = err.details.body[0].message;
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json({error: true, message: message});
    }    
    next();
  }, 
  (req, res, next) => {
    res.data = Object.assign({},fighter, req.body);
    next();
  }   
];

const updateFighterValid = [
  validate(updateFighter, {}, {}),
  (err, req, res, next) => {
    const message = err.details.body[0].message;
    if (err instanceof ValidationError) {
      return res.status(err.statusCode).json({error: true, message: message});
    }    
    next();
  }
];

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;