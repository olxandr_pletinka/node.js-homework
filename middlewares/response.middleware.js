const responseMiddleware = (req, res, next) => {
    
  if (!res.err) {
    res.status(200).json(res.data)
  } else {
    res.status(res.err.status || 400).json({error: true, message: res.err.message}) 
  }
}

exports.responseMiddleware = responseMiddleware;